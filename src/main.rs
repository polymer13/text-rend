extern crate sdl2;

#[macro_use]
pub mod render_manager;
pub mod app;
pub mod text;

use render_manager::RenderPass;
use rusttype::Font;
#[allow(dead_code)]
const DEFAULT_FONT_DATA: &[u8] = include_bytes!("/usr/share/fonts/truetype/liberation/LiberationSans-Bold.ttf");
const ANOTHER_FONT_DATA: &[u8] = include_bytes!("/usr/share/fonts/truetype/lyx/eufm10.ttf");

struct ClearPass (u8, u8, u8);

impl RenderPass<sdl2::video::Window> for ClearPass {

    fn init(&mut self) {
    }

    fn draw(&mut self, _delta : std::time::Duration, canvas: &mut sdl2::render::Canvas<sdl2::video::Window>) {
        canvas.set_draw_color(sdl2::pixels::Color::RGB(self.0, self.1, self.2));
        canvas.clear();
    }

    fn get_name(&self) -> &'static str {
        &"Clear"
    }
}

struct TextRender {
    font: Font<'static>,
    text: text::TextLine,
    time: f64,
}

impl RenderPass<sdl2::video::Window> for TextRender {

    fn init(&mut self) {
    }

    fn draw(&mut self, delta: std::time::Duration, canvas: &mut sdl2::render::Canvas<sdl2::video::Window>) {

        use sdl2::pixels::PixelFormatEnum;
        use sdl2::render::TextureAccess;
        use sdl2::rect::Rect;

        let delta =  delta.as_millis() as f64 * 0.001;
        self.time += delta;

        let r = ( 128.0 + (120.0 * ( self.time * 3.15 ).sin()) )  as u8;
        let g = ( 128.0 + (80.0 * ( self.time * 3.15 * 2.0).sin()) )  as u8;
        let b = ( 128.0 + (80.0 * ( self.time * 3.15 * 3.0).sin()) )  as u8;
        self.text.color = text::Color::rgb( r, g, b);

        self.text.render_text (
            "This is some text on screen!",
            self.font.clone()
        );

        // Rotation of text
        let speed = 0.5;
        let pos = text::Position::new(
            ( 300.0 + (( self.time * speed ).sin()*150.0) - (( self.time *speed ).cos()*150.0) ) as i32,
            ( 300.0 + (( self.time *speed ).sin()*150.0) + (( self.time *speed ).cos()*150.0) ) as i32,
        );

        let (width, height, pixels) = self.text.get_pixels().unwrap();
        canvas.set_blend_mode(sdl2::render::BlendMode::Add);
        let tc = canvas.texture_creator();

        let mut texture = tc.create_texture(
            PixelFormatEnum::RGBA32,
            TextureAccess::Target,
            width, height
        ).unwrap();

        texture.set_blend_mode(sdl2::render::BlendMode::Add);

        texture.update(
            None,
            pixels.as_slice(),
            ( width * 4) as usize
        ).unwrap();

        canvas.copy_ex(
            &texture, None,
            Rect::new(pos.x, pos.y, width, height),
            0.0, None, false, false
        ).unwrap();
    }

    fn get_name(&self) -> &'static str {
        &"TextLine"
    }
}

fn main() {

    // let default_font = Font::try_from_bytes(&DEFAULT_FONT_DATA[..]).unwrap();
    let default_font = Font::try_from_bytes(&ANOTHER_FONT_DATA[..]).unwrap();
    {
        let t = text::TextLine::builder()
            .with_scale(25.0)
            .with_color(text::Color::rgb(128,255,128))
            .build();

        let conf = app::AppConf {
            window_title: "Hello There!".to_owned(),
            window_resolution: (800, 600),
        };

        let mut _app = match app::AppCore::init(conf) {
            Err(err) => {
                panic!(format!("Fatal error: {}", err));
            },
            Ok(app) => app,
        };

        _app.render_manager.add_render_pass(
            render_pass!(ClearPass(0,0,0)
            ));

        _app.render_manager.add_render_pass(
                Box::new(TextRender {
                    font: default_font.clone(),
                    text: t,
                    time: 0.0,
                }) as Box<dyn RenderPass<sdl2::video::Window>>
        );

        let mut event_pump = _app.sdl_ctx.event_pump().unwrap();

        'render_loop: loop {

            _app.render_manager.render();

            for event in event_pump.poll_iter() {
                if let  sdl2::event::Event::Quit {..} = event {
                    break 'render_loop;
                }
            }
        }
    }
}
