use std::boxed::Box;
use sdl2::render::Canvas;
use sdl2::render::RenderTarget;
use std::time::{Instant, Duration};

// Render pass is executed every frame to draw a layer on the canvas by
// manipulating individual pixels
pub trait RenderPass<T: RenderTarget> {
    fn init(&mut self);
    fn draw(&mut self, delta: Duration, pixels: &mut Canvas<T> );
    fn get_name(&self) -> &str;
}

pub struct RenderManager<T: RenderTarget> {
    canvas: Canvas<T>,
    render_passes: Vec<( Box<dyn RenderPass<T>>, std::time::Instant )>,
}


impl<T: sdl2::render::RenderTarget> RenderManager<T> {

    pub fn from_sdl_canvas(c: Canvas<T>) -> RenderManager<T> {
        Self {
            canvas: c,
            render_passes: Vec::<(Box<dyn RenderPass<T>>, Instant)>::new(),
        }
    }

    pub fn render(&mut self) {

        let t_start = Instant::now();
        for (pass, last_delta) in self.render_passes.iter_mut() {
            let t = Instant::now();
            let delta = t - *last_delta;
            pass.draw(delta, &mut self.canvas);
            *last_delta = t;
        }
        let t_stop = Instant::now();

        std::thread::sleep(
            Duration::from_millis(1000/60)
                .checked_sub(t_stop - t_start)
                .unwrap_or(Duration::from_nanos(0))
        );

        self.canvas.present();
    }

    pub fn add_render_pass(&mut self, mut pass: Box<dyn RenderPass<T>>) {
        pass.init();
        self.render_passes.push((pass, Instant::now()));
    }
}

macro_rules! render_pass{
    ($e:expr) => {
        // Box::new($e) as Box<dyn RenderPass<sdl2::video::Window>>
        Box::new($e) as Box<dyn RenderPass<_>>
    }
}
