use crate::render_manager::{RenderManager};
use std::rc::Rc;
use sdl2::video::Window;

#[allow(dead_code)]
pub struct AppCore {
    pub sdl_ctx: sdl2::Sdl,
    pub sdl_video: sdl2::VideoSubsystem,
    pub window_ctx: Rc<sdl2::video::WindowContext>,
    pub render_manager: RenderManager<Window>,
}

pub struct AppConf {
    pub window_title: String,
    pub window_resolution: (u32, u32),
}

impl AppCore {

    pub fn init(conf: AppConf) -> Result<Self, String> {

        use sdl2::video::WindowBuildError as Wbe;
        use sdl2::IntegerOrSdlError as Iose;

        let sdl = sdl2::init()?;
        let video = sdl.video()?;
        let win = match video.window(
            conf.window_title.as_str(),
            conf.window_resolution.0,
            conf.window_resolution.1,
        ).position_centered()
         .build() {

             Ok(win) => win,
             Err(err) => match err {
                 Wbe::HeightOverflows(i) =>
                     return Err(format!( "Window height is too large: {}", i) ),
                 Wbe::WidthOverflows(i) =>
                     return Err(format!( "Window width is too large: {}", i) ),
                 Wbe::InvalidTitle(_) =>
                     return Err("Window title is invalid!".to_owned()),
                 Wbe::SdlError(err) =>
                     return Err(format!("SDL internal error: {}", err)),
             }
         };

        let win_ctx = win.context();

        let c = match win.into_canvas().build() {
            Ok(c) => c,
            Err(err) => match err {
                Iose::SdlError(err) =>
                    return Err(format!("SDL internal error during canvas creation: {}", err)),
                Iose::IntegerOverflows(e, n) =>
                    return Err(format!("SDL encountered integer overflow during canvas creation: {} VALUE({})", e, n)),
            }
        };

        Ok( Self {
            sdl_ctx: sdl,
            sdl_video: video,
            window_ctx: win_ctx,
            render_manager: RenderManager::from_sdl_canvas(c),
        })
    }

}
